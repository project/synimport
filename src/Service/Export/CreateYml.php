<?php

namespace Drupal\synimport\Service\Export;

use Drupal\synimport\Service\Export\CreateYmlBase;
use Drupal\synimport\Service\Logger;

/**
 * CreateYml class.
 */
class CreateYml extends CreateYmlBase {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * Available entity types.
   *
   * @var array
   */
  protected $entityTypes;

  /**
   * Available bundles by entity types.
   *
   * @var array
   */
  protected $bundles;

  /**
   * Available bundles by entity types.
   *
   * @var string
   */
  protected $exportingStatus;

  /**
   * Available bundles by entity types.
   *
   * @var string
   */
  protected $exportingEntityType;

  /**
   * Available bundles by entity types.
   *
   * @var array
   */
  protected $exportingBundles;

  /**
   * Database query.
   *
   * @var object
   */
  protected $query;

  /**
   * Additional entities for import.
   *
   * @var array
   */
  protected $additionalExport;

  /**
   * Path to export directory.
   *
   * @var string
   */
  protected $exportDir;

  /**
   * Service constructor.
   * @todo Получать папку, как параметр из drush команды
   */
  public function __construct() {
    $this->entityTypes = $this->entityTypes();
    $this->bundles = $this->bundles($this->entityTypes);
    $this->exportingBundles = [];
    $this->exportingEntityType = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
    parent::__construct($this->log);
  }

  /**
   * Main func.
   */
  public function createYmls(string $export_dir, string $bundle, string | int $status) {
    $this->exportDir = $export_dir;
    $this->exportingStatus = $status;
    $this->checkBundle($bundle);
    $entities_data = $this->getEntitiesData();
    $entity_count = count($entities_data);
    $this->log->info("Got $entity_count entities for export");
    $this->log->head("Creating ymls...");
    foreach ($entities_data as $id => $entity) {
      $yml_data = $this->getYml($entity);
      $this->log->info("Yml data received");
      if (!empty($yml_data)) {
        $filename = "$id-{$this->exportingEntityType}";
        $this->log->info("Creating $filename.yml");
        $this->createYml("{$this->exportDir}/{$this->exportingEntityType}", $filename, $yml_data);
        $this->log->notice("Exported $filename");
      }
      else {
        $this->log->warning("Yml data is empty");
      }
    }
    $this->log->success("Main export finished");
    $this->provideAdditionalExport();
  }

  /**
   * Creates yml from a single entity.
   */
  private function getYml(object $entity) : array {
    $yml_data = $this->getEntityYml($entity);
    if (empty($yml_data)) {
      $this->log->warning("Empty yml received");
    }
    else {
      if ($this->exportingEntityType == 'taxonomy_term') {
        $yml_data['vid'] = $entity->bundle();
      }
      else {
        $yml_data['type'] = $entity->bundle();
      }
      $yml_data['uid'] = 1;
    }
    return $yml_data;
  }

  /**
   * Data from object to the specified array.
   */
  private function getEntityYml(object $entity) : array {
    $yml_data = [];
    $bundle = $entity->bundle();
    $field_definitions = $this->fieldDefinitions($entity->getEntityTypeId(), $bundle);
    foreach ($field_definitions as $field_data) {
      if ($field_data['is_reference']) {
        $ids = $entity->{$field_data['name']}->getValue();
        $storage = \Drupal::entityTypeManager()->getStorage($field_data['target_type']);
        $yml_data[$field_data['name']]['type'] = $field_data['type'];
        $yml_data[$field_data['name']]['content'] = [];
        foreach ($ids as $id) {
          if ($field_data['type'] != 'related') {
            $sub_entity = $storage->load($id['target_id']);
            if ($sub_entity) {
              $yml_data[$field_data['name']]['content'][] = $this->getEntityYml($sub_entity);
            }
          }
          else {
            if ($id['target_id'] != 0) {
              $yml_data[$field_data['name']]['content'][] = "/{$field_data['target_type']}" . "/{$id['target_id']}-{$field_data['target_type']}";
            }
          }
          $this->setAdditionalExport($field_data, $id['target_id']);
        }
      }
      else if (in_array($field_data['type'], ['attach', 'image'])) {
        $yml_data[$field_data['name']]['type'] = $field_data['type'];
        $yml_data[$field_data['name']]['files'] = $entity->{$field_data['name']}->getValue();
      }
      else {
        $yml_data[$field_data['name']] = $entity->{$field_data['name']}->value ?? '';
      }
      $this->addTypeFeatures($field_data, $yml_data, $entity, "{$this->exportDir}/{$this->exportingEntityType}");
    }
    return $yml_data;
  }

  /**
   * Get data for export.
   */
  private function getEntitiesData() : array {
    $this->log->head("Getting entities data...");
    $this->query = $this->initQuery($this->exportingEntityType, $this->exportingStatus, $this->exportingBundles);
    $entities = [];
    try {
      $storage = \Drupal::entityTypeManager()->getStorage($this->exportingEntityType);
    } catch (\Throwable $th) {
      $this->log->error("Storage error");
      $this->log->error($th->getMessage());
      exit;
    }
    try {
      $entities = $storage->loadMultiple($this->query->execute());
    } catch (\Throwable $th) {
      $this->log->error("Getting entities error");
      $this->log->error($th->getMessage());
      exit;
    }
    if (empty($entities)) {
      $this->log->warning("Nothing to export ({$this->exportingEntityType})");
      exit;
    }
    return $entities;
  }

  /**
   * Export additional entities.
   */
  private function provideAdditionalExport() {
    if (!empty($this->additionalExport)) {
      foreach ($this->additionalExport as $entity_type => $ids) {
        $this->exportingEntityType = $entity_type;
        $this->exportingBundles = [];
        $this->checkBundle($entity_type);
        $entities_data = $this->getAdditionalEntitiesData($ids);
        foreach ($entities_data as $id => $entity) {
          $yml_data = $this->getYml($entity);
          $this->log->info("Yml data received");
          if (!empty($yml_data)) {
            $filename = "$id-{$this->exportingEntityType}";
            $this->log->info("Creating $filename.yml");
            $this->createYml("{$this->exportDir}/{$this->exportingEntityType}", $filename, $yml_data);
            $this->log->notice("Exported $filename");
          }
          else {
            $this->log->warning("Yml data is empty");
          }
        }
      }
    }
    else {
      $this->log->notice("Nothing added for additional export");
    }
  }

  /**
   * Add entities for additional export.
   */
  private function setAdditionalExport(array $field_data, string $entity_id) {
    $types_for_import = [
      'commerce_product',
      'node',
      'taxonomy_term',
      'block',
      'file',
    ];
    if (
      $field_data['target_type'] !== $this->exportingEntityType &&
      in_array($field_data['target_type'], $types_for_import)
    ) {
      $this->additionalExport[$field_data['target_type']][$entity_id] = $entity_id;
    }
  }

  /**
   * Get additional data for export.
   */
  private function getAdditionalEntitiesData(array $ids) : array {
    $this->log->head("Getting additional entities data...");
    $entities = [];
    try {
      $storage = \Drupal::entityTypeManager()->getStorage($this->exportingEntityType);
    } catch (\Throwable $th) {
      $this->log->error("Storage error");
      $this->log->error($th->getMessage());
      exit;
    }
    try {
      $entities = $storage->loadMultiple($ids);
    } catch (\Throwable $th) {
      $this->log->error("Getting entities error");
      $this->log->error($th->getMessage());
      exit;
    }
    if (empty($entities)) {
      $this->log->warning("Nothing to export ({$this->exportingEntityType})");
    }
    return $entities;
  }

  /**
   * Check bundle availability.
   */
  private function checkBundle($bundle) : bool {
    if (in_array($bundle, $this->entityTypes)) {
      $this->exportingEntityType = $bundle;
      foreach ($this->bundles[$bundle] as $entity_bundle => $bundle_data) {
        $this->exportingBundles[] = $entity_bundle;
      }
    }
    else {
      foreach ($this->bundles as $entity_type => $entity_bundles) {
        foreach ($entity_bundles as $entity_bundle => $bundle_data) {
          if ($bundle == $entity_bundle) {
            $this->exportingEntityType = $entity_type;
            $this->exportingBundles[] = $entity_bundle;
          }
        }
      }
    }
    if (!$this->exportingEntityType) {
      $this->log->error("Non-supported entity type or bundle");
      return FALSE;
    }
    $this->log->notice("Selected entity type: {$this->exportingEntityType}");
    $bundles_string = 'Selected bundles: ';
    foreach ($this->exportingBundles as $entity_bundle) {
      $bundles_string .= "$entity_bundle; ";
    }
    $this->log->notice($bundles_string);
    return TRUE;
  }

}
