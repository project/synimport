<?php

namespace Drupal\synimport\Service\Export;

use Drupal\synimport\Service\Logger;

/**
 * Defines Content Export.
 *
 * @internal
 *   For internal usage by the Commerce synimport module.
 */

/**
 * Export service.
 */
class Export {
  /**
   * Global log variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * Menu service.
   *
   * @var Menu
   */
  protected $menuExport;

  /**
   * Taxonomy service.
   *
   * @var Taxonomy
   */
  protected $taxonomyExport;

  /**
   * Node service.
   *
   * @var Node
   */
  protected $nodeExport;

  /**
   * Product service.
   *
   * @var Product
   */
  protected $productExport;

  /**
   * Block service.
   *
   * @var Block
   */
  protected $blockExport;

  /**
   * Redis service.
   *
   * @var Redis
   */
  protected $redisExport;

  /**
   * Service constructor.
   *
   * @param Menu $menuExport
   * @param Taxonomy $taxonomyExport
   * @param Node $nodeExport
   * @param Product $productExport
   * @param Block $blockExport
   * @param Redis $redisExport
   */
  public function __construct(
    Taxonomy $taxonomyExport,
    Node $nodeExport,
    Product $productExport,
    Block $blockExport,
  ) {
    $this->taxonomyExport = $taxonomyExport;
    $this->nodeExport = $nodeExport;
    $this->productExport = $productExport;
    $this->blockExport = $blockExport;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Export Full Content.
   */
  public function export(string $export_dir, string $status) {
    $this->log->head('Start Taxonomy Export');
    $this->taxonomyExport->setLogger($this->log);
    $this->taxonomyExport->export($export_dir, 'taxonomy_term', $status);
    $this->log->success('Done Taxonomy Export');

    $this->log->head('Start Node Export');
    $this->nodeExport->setLogger($this->log);
    $this->nodeExport->export($export_dir, 'node', $status);
    $this->log->success('Done Node Export');

    $this->log->head('Start Product Export');
    $this->productExport->setLogger($this->log);
    $this->productExport->export($export_dir, 'commerce_product', $status);
    $this->log->success('Done Product Export');

    $this->log->head('Start Block Export');
    $this->blockExport->setLogger($this->log);
    $this->blockExport->export($export_dir, 'block', $status);
    $this->log->success('Done Block Export');

  }

  /**
   * Export Taxonomy.
   */
  public function exportTaxonomy(string $export_dir, string $bundle, string $status) {
    $this->log->head('Start Taxonomy Export');
    $this->taxonomyExport->setLogger($this->log);
    $this->taxonomyExport->export($export_dir, $bundle, $status);
    $this->log->success('Done Taxonomy Export');
  }

  /**
   * Export Node.
   */
  public function exportNode(string $export_dir, string $bundle, string $status) {
    $this->log->head('Start Node Export');
    $this->nodeExport->setLogger($this->log);
    $this->nodeExport->export($export_dir, $bundle, $status);
    $this->log->success('Done Node Export');
  }

  /**
   * Export Products.
   */
  public function exportProducts(string $export_dir, string $bundle, string $status) {
    $this->log->head('Start Product Export');
    $this->productExport->setLogger($this->log);
    $this->productExport->export($export_dir, $bundle, $status);
    $this->log->success('Done Product Export');
  }

  /**
   * Export Blocks.
   */
  public function exportBlocks(string $export_dir, string $bundle, string $status) {
    $this->log->head('Start Block Export');
    $this->blockExport->setLogger($this->log);
    $this->blockExport->export($export_dir, $bundle, $status);
    $this->log->success('Done Block Export');
  }

}
