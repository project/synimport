<?php

namespace Drupal\synimport\Utility;

/**
 * Директория пуста.
 */
class EmptyDirException extends \Exception {

  /**
   * Short descr.
   *
   * @param string $message
   *   Сообщение.
   */
  public function __construct(string $message) {
    \Exception::__construct($message);
  }

}
