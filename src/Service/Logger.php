<?php

namespace Drupal\synimport\Service;

use Symfony\Component\Process\Process;

/**
 * Log messages.
 */
class Logger {
  /**
   * Console message color.
   *
   * @var array
   */
  protected $colors = [];

  /**
   * Set up shell colors.
   */
  public function __construct() {
    $this->colors['black'] = '0;30';
		$this->colors['dark_gray'] = '1;30';
		$this->colors['blue'] = '0;34';
		$this->colors['light_blue'] = '1;34';
		$this->colors['green'] = '0;32';
		$this->colors['light_green'] = '1;32';
		$this->colors['cyan'] = '0;36';
		$this->colors['light_cyan'] = '1;36';
		$this->colors['orange'] = '0;31';
		$this->colors['red'] = '1;31';
		$this->colors['purple'] = '0;35';
		$this->colors['light_purple'] = '1;35';
		$this->colors['brown'] = '0;33';
		$this->colors['yellow'] = '1;33';
		$this->colors['light_gray'] = '0;37';
		$this->colors['white'] = '1;37';
  }

  /**
   * Usual message.
   */
  public function message(string $msg) {
    $this->log($msg);
    $this->send($msg);
  }

  /**
   * Redd error message.
   */
  public function error(string $msg) {
    $prefix = $this->setColor('[ERROR]', 'red');
    $this->log("\n$prefix $msg\n");
    $this->send("\n[ERROR] $msg\n");
  }

  /**
   * Yellow warning message.
   */
  public function warning(string $msg) {
    $prefix = $this->setColor('[WARNING]', 'yellow');
    $this->log("\n$prefix $msg\n");
    $this->send("\n[WARNING] $msg\n");
  }

  /**
   * Blue notice message.
   */
  public function head(string $msg) {
    $prefix = $this->setColor('[HEAD]', 'light_purple');
    $this->log("\n$prefix $msg\n");
    $this->send("\n[HEAD] $msg\n");
  }

  /**
   * Blue notice message.
   */
  public function info(string $msg) {
    $prefix = $this->setColor('[INFO]', 'purple');
    $this->log("$prefix $msg");
    $this->send("[INFO] $msg");
  }

  /**
   * Blue notice message.
   */
  public function notice(string $msg) {
    $prefix = $this->setColor('[NOTICE]', 'light_blue');
    $this->log("$prefix $msg");
    $this->send("[NOTICE] $msg");
  }

  /**
   * Green success message.
   */
  public function success(string $msg) {
    $prefix = $this->setColor('[SUCCESS]', 'light_green');
    $this->log("\n$prefix $msg\n");
    $this->send("\n[SUCCESS] $msg\n");
  }

  /**
   * Returns colored string.
   */
  public function setColor($string, $color = NULL) {
    $colored_string = "";
    if (isset($this->colors[$color])) {
      $colored_string .= "\033[" . $this->colors[$color] . "m";
    }
    $colored_string .= $string . "\033[0m";
    return $colored_string;
  }

  /**
   * Send to docker.
   */
  public function send(string $msg) {
    $timeout = 60000;
    $cmd = ["cd ../console/ && ./console.php tpl:echo '$msg'"];
    $process = new Process($cmd);
    $process->setTimeout($timeout);
    $process->start();
    $process->wait();
    return [
      'success' => $process->isSuccessful(),
      'output' => $process->getOutput(),
      'error' => $process->getErrorOutput(),
    ];
  }

  /**
   * Log to console.
   */
  public function log(string $msg) {
    print_r($msg);
    print_r("\n");
  }

}
