<?php

namespace Drupal\synimport\Service\Import;

use Drupal\Core\Serialization\Yaml;
use Drupal\synimport\Service\Logger;

/**
 * Product import class.
 *
 * @internal
 *   For internal usage by the Commerce synimport module.
 */
class Product {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * Global directory variable.
   *
   * @var string
   */
  protected $directory;

  /**
   * CreateEntity Service variable.
   *
   * @var CreateEntity
   */
  protected $createEntity;

  /**
   * Service constructor.
   *
   * @param CreateEntity $createEntity
   */
  public function __construct(CreateEntity $createEntity) {
    $this->createEntity = $createEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Import Products.
   */
  public function import(string $directory) {
    $sourceProducts = scandir($directory);
    foreach ($sourceProducts as $file_name) {
      if (!is_dir($file_name)) {
        if (mb_substr($directory, -1) == '/') {
          $path = $directory . $file_name;
        } else {
          $path = $directory . '/' . $file_name;
        }
        $product_array = Yaml::decode(file_get_contents($path));
        if (is_array($product_array)) {
          $this->createEntity->setLogger($this->log);
          $this->createEntity->updateCreateEntity($product_array, $directory, $path);
        }
      }
    }
  }

}
