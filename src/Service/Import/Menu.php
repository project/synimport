<?php

namespace Drupal\synimport\Service\Import;

use Drupal\Core\Serialization\Yaml;
use Drupal\synimport\Service\Logger;

/**
 * Menu import class.
 *
 * @internal
 *   For internal usage by the Commerce synimport module.
 */
class Menu {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * CreateEntity Service variable.
   *
   * @var CreateEntity
   */
  protected $createEntity;

  /**
   * Service constructor.
   *
   * @param CreateEntity $createEntity
   */
  public function __construct(CreateEntity $createEntity) {
    $this->createEntity = $createEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Import Menu.
   */
  public function import(string $directory) {
    $sourceMenu = scandir($directory);
    foreach ($sourceMenu as $file_name) {
      if (!is_dir($file_name)) {
        if (mb_substr($directory, -1) == '/') {
          $path = $directory . $file_name;
        } else {
          $path = $directory . '/' . $file_name;
        }
        $data = Yaml::decode(file_get_contents($path));
        if ($menu = \Drupal::entityTypeManager()->getStorage('menu')->load($data['menu']['id'])) {
          /** @var \Drupal\system\MenuInterface $menu */
          $menu->set('label', $data['menu']['label']);
          $menu->set('description', $data['menu']['description']);
          $menu->save();
        }
        else {
          \Drupal::entityTypeManager()->getStorage('menu')->create([
            'uid' => 1,
            'id' => $data['menu']['id'],
            'label' => $data['menu']['label'],
            'description' => $data['menu']['description'],
          ])->save();
        }
        $this->addMenuLink($data['links'], NULL, $data['menu']['id']);
      }
    }
  }

  /**
   * Add Menu Links.
   */
  private function addMenuLink($links, $parent_id, $menu_id) {
    if (empty($links)) {
      return;
    }
    $link = \Drupal::entityTypeManager()->getStorage('menu_link_content');
    foreach ($links as $menu_link) {
      $t = [
        'uid' => 1,
        'title' => $menu_link['title'],
        'link' => ['uri' => $menu_link['uri']],
        'menu_name' => $menu_id,
        'expanded' => $menu_link['expanded'],
        'weight' => $menu_link['weight'],
        'parent' => $parent_id,
        'description' => $menu_link['description'],
      ];
      $children = $menu_link['children'];
      $menu_link = $link->create($t);
      $menu_link->save();
      if (!empty($children[0])) {
        $bundle = $menu_link->bundle();
        $uuid = $menu_link->uuid();
        $parent_id1 = $bundle . ":" . $uuid;
      }
      else {
        $parent_id1 = NULL;
      }
      $this->addMenuLink($children, $parent_id1, $menu_id);
    }
  }

}
