<?php

namespace Drupal\synimport\Service\Import;

use Drupal\Core\Serialization\Yaml;
use Drupal\synimport\Service\Logger;

/**
 * Node import class.
 *
 * @internal
 *   For internal usage by the Commerce synimport module.
 */
class Node {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * CreateEntity Service variable.
   *
   * @var CreateEntity
   */
  protected $createEntity;

  /**
   * Service constructor.
   *
   * @param CreateEntity $createEntity
   */
  public function __construct(CreateEntity $createEntity) {
    $this->createEntity = $createEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Import Nodes.
   */
  public function import(string $directory) {
    $sourceNodes = scandir($directory);
    foreach ($sourceNodes as $file_name) {
      if (!is_dir($file_name)) {
        if (mb_substr($directory, -1) == '/') {
          $path = $directory . $file_name;
        } else {
          $path = $directory . '/' . $file_name;
        }
        $node_array = Yaml::decode(file_get_contents($path));
        if (is_array($node_array)) {
          $this->createEntity->setLogger($this->log);
          $this->createEntity->updateCreateEntity($node_array, $directory, $path);
        }
      }
    }
  }

}
