<?php

namespace Drupal\synimport\Service\Import;

use Drupal\Core\Serialization\Yaml;
use Drupal\synimport\Service\Logger;

/**
 * Taxomy import class.
 *
 * @internal
 *   For internal usage by the Commerce synimport module.
 */
class Taxonomy {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * CreateEntity Service variable.
   *
   * @var CreateEntity
   */
  protected $createEntity;

  /**
   * Service constructor.
   *
   * @param CreateEntity $createEntity
   */
  public function __construct(CreateEntity $createEntity) {
    $this->createEntity = $createEntity;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Import Taxonomy.
   *
   * @todo Сделай что-то с путем файла для словарей
   */
  public function import(string $directory) {
    $sourceTaxonomy = scandir($directory);
    foreach ($sourceTaxonomy as $file_name) {
      if (!is_dir($file_name)) {
        if (mb_substr($directory, -1) == '/') {
          $path = $directory . $file_name;
        } else {
          $path = $directory . '/' . $file_name;
        }
        $term_array = Yaml::decode(file_get_contents($path));
        if (is_array($term_array)) {
          $this->createEntity->setLogger($this->log);
          $this->createEntity->updateCreateEntity($term_array, $directory, $path);
        }
      }
    }
  }

}
