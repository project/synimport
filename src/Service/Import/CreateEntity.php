<?php

namespace Drupal\synimport\Service\Import;

use Drupal\pathauto\PathautoState;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\synimport\Service\Logger;

/**
 * CreateEntity class.
 */
class CreateEntity {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * Files Service variable.
   *
   * @var Files
   */
  protected $filesImport;

  /**
   * Core directory variable.
   *
   * @var string
   */
  protected $coreDir;

  /**
   * Available bundles.
   *
   * @var array
   */
  protected $bundles;

  /**
   * Fields map.
   *
   * @var array
   */
  protected $map;

  /**
   * Available entity types.
   *
   * @var array
   */
  protected $entityTypes;

  /**
   * Service constructor.
   *
   * @param Files $filesImport
   */
  public function __construct(Files $filesImport) {
    $this->map = [];
    $this->entityTypes = $this->getEntityTypes();
    $this->bundles = $this->getBundles($this->entityTypes);
    $this->coreDir = '';
    $this->filesImport = $filesImport;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Update or Create Products.
   */
  public function updateCreateEntity(array $entity_array, string $directory, string $file_path) {
    $this->filesImport->setDir($directory);
    $this->coreDir = mb_substr(trim($directory), 0, mb_strripos(trim($directory), '/'));
    $storage = $this->getStorage($entity_array);
    if ($storage) {
      $created_entity = $this->getEntityFromBase($file_path, $storage);
      if (!$created_entity) {
        $created_entity = $this->createEntity($entity_array, $file_path, 'full_create');
      }
      $this->setRelated($created_entity->id(), $entity_array);
    }
  }

  /**
   * Update or Create Products.
   */
  private function createEntity(array $entity_array, string $file_path, string $statement = 'full_create') {
    if ($statement == 'full_create') {
      $created_entity = $this->create($entity_array);
      if ($created_entity) {
        $this->map[$file_path] = $created_entity->id();
      }
    } elseif ($statement == 'only_as_related') {
      $storage = $this->getStorage($entity_array);
      $created_entity = $this->getEntityFromBase($file_path, $storage);
      if ($created_entity) {
        return $created_entity;
      }
      $created_entity = $this->create($entity_array);
      if ($created_entity) {
        $this->map[$file_path] = $created_entity->id();
      }
    }
    return $created_entity;
  }

  /**
   * CreateEntity.
   */
  public function create(array $entity_array) {
    $storage = $this->getStorage($entity_array);
    if ($storage) {
      $storage_name = $storage->getEntityTypeId();
      $result = [
        'uid' => 1,
      ];
      $result = $this->arrayAssembly($entity_array, $result);
      if ($result) {
        $entity = $storage->create($result);
        $entity->save();
      }
      $this->afterSave($entity, $entity_array, $storage_name);

      $entity_title = str_replace("\n", " ", trim($entity->label()));
      $entity_id = $entity->id();
      $entity_type_id = $entity->getEntityTypeId();

      $this->log->notice("$entity_type_id: $entity_title [$entity_id]");
      return $entity;
    }
    return NULL;
  }

  /**
   * CreateEntity.
   */
  public function createParagraph(array $entity_array) {
    $paragraph = $this->create($entity_array);
    if ($paragraph) {
      $paragraph_id = $paragraph->id();
      $this->setRelated($paragraph_id, $entity_array);
    }
    return $paragraph;
  }

  /**
   * Actions right after create-save entity.
   */
  private function afterSave($entity, $entity_array, $storage_name) {
    $entity_id = $entity->id();
    if ($storage_name == 'node') {
      if (!empty($entity_array['path'])) {
        $entity->path = [
          'alias' => $entity_array['path'],
          'pathauto' => PathautoState::SKIP,
        ];
      }
      if (array_key_exists('is_front', $entity_array) && $entity_array['is_front'] == TRUE) {
        $config = \Drupal::service('config.factory')->getEditable('system.site');
        $config->set('page.front', "/node/$entity_id");
        $config->save();
      }
    }
  }

  /**
   * PreparesArray.
   */
  private function arrayAssembly(array $entity_array, array $result) {
    foreach ($entity_array as $key => $value) {
      if (!empty($value['type'])) {
        switch ($value['type']) {
          case 'image':
            $result[$key] = $this->filesImport->importImages($value['content']);
            break;

          case 'attach':
            $result[$key] = $this->filesImport->importAttach($value['content']);
            break;

          case 'media':
            $result[$key] = $this->filesImport->importMedia($value['content']);
            break;

          case 'taxonomy':
            if (is_array($value['content'])) {
              $terms = [];
              foreach ($value['content'] as $term_name) {
                $term = $this->importTaxonomyField($term_name);
                $terms[] = $term;
              }
              $result[$key] = $terms;
            } else {
              $result[$key] = $this->importTaxonomyField($value['content']);
            }
            break;

          case 'paragraph':
            $paragraphs = [];
            foreach ($value['content'] as $paragraph_array) {
              /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
              $paragraph = $this->createParagraph($paragraph_array);
              if ($paragraph) {
                $paragraphs[] = [
                  'target_id' => $paragraph->id(),
                  'target_revision_id' => $paragraph->getRevisionId(),
                ];
              }
            }
            $result[$key] = $paragraphs;
            break;

          case 'variations':
            $variations = [];
            foreach ($value['content'] as $variation_array) {
              $variation = $this->create($variation_array);
              if ($variation) {
                $variations[] = $variation->id();
              }
            }
            $result[$key] = $variations;
            break;

          case 'attribute':
            $value['content']['attribute'] = $key;
            $ids = $this->issetAttribute($value['content']);
            if (!empty($ids)) {
              $storage = \Drupal::entityTypeManager()->getStorage('commerce_product_attribute_value');
              $attribute = $storage->load(array_shift($ids));
            } else {
              $attribute = $this->create($value['content']);
            }
            if ($attribute) {
              $result["attribute_$key"] = $attribute->id();
            }
            break;

          default:
            $result[$key] = $value['content'];
            break;
        }
      } else {
        $result[$key] = $value;
      }
    }
    return $result;
  }

  /**
   * Create Related Products.
   */
  private function setRelated(string $created_entity_id, array $created_entity_array) {
    $related_fields = [];
    $parent_storage = $this->getStorage($created_entity_array);
    foreach ($created_entity_array as $field_name => $field_data) {
      if (is_array($field_data) && in_array('type', array_keys($field_data)) && $field_data['type'] == 'related') {
        $related_fields[] = [
          'field_name' => $field_name,
          'field_content' => $field_data['content'],
        ];
      }
    }
    if (!empty($related_fields)) {
      foreach ($related_fields as $related_data) {
        $entities_ids = [];
        foreach ($related_data['field_content'] as $file_path) {
          $entity_array = $this->getEntityFromFile($file_path);
          if (!empty($entity_array)) {
            $storage = $this->getStorage($entity_array);
            $created_entity = $this->getEntityFromBase($this->coreDir . $file_path . '.yml', $storage);
            if ($created_entity) {
              $entities_ids[] = $created_entity->id();
            } else {
              $related_entity_id = $this->createEntity($entity_array, $this->coreDir . $file_path . '.yml', 'only_as_related')->id();
              $entities_ids[] = $related_entity_id;
              $this->map[$this->coreDir . $file_path . '.yml'] = $related_entity_id;
            }
          } else {
            $this->log->warning("Undefined file: $file_path");
          }
        }
        $created_entity = $parent_storage->load($created_entity_id);
        $created_entity->set($related_data['field_name'], $entities_ids);
        $created_entity->save();
      }
    }
  }

  /**
   * Get storage.
   */
  private function getStorage(array $entity_array) {
    $storage_name = 'undefined';
    if (!empty($entity_array['entity_type'])) {
      $storage_name = $entity_array['entity_type'];
    } elseif (empty($entity_array['type']) && empty($entity_array['vid']) && empty($entity_array['attribute'])) {
      $storage_name = 'none_type_specified';
    } elseif ($this->bundles) {
      if (!empty($entity_array['type'])) {
        foreach ($this->bundles as $entity_type => $entity_bundles) {
          if (in_array($entity_array['type'], array_keys($entity_bundles))) {
            $storage_name = $entity_type;
            break;
          }
        }
      } elseif (!empty($entity_array['vid'])) {
        foreach ($this->bundles as $entity_type => $entity_bundles) {
          if (!$this->checkVocabularyExistence($entity_array['vid'])) {
            $this->create([
              'type' => 'taxonomy_vocabulary',
              'vid' => $entity_array['vid'],
            ]);
          }
          if (in_array($entity_array['vid'], array_keys($entity_bundles))) {
            $storage_name = $entity_type;
            break;
          }
        }
      } else {
        foreach ($this->bundles as $entity_type => $entity_bundles) {
          if (in_array($entity_array['attribute'], array_keys($entity_bundles))) {
            $storage_name = $entity_type;
            break;
          }
        }
      }
    }
    switch ($storage_name) {
      case 'undefined':
        $type = $entity_array['type'];
        $keys = array_keys($entity_array);
        if (in_array('title', $keys)) {
          $in = " in {$entity_array['title']}";
        } elseif (in_array('name', $keys)) {
          $in = " in {$entity_array['name']}";
        } else {
          $in = "";
        }
        $this->log->error("Non-supported entity type ($type)$in");
        break;

      case 'none_type_specified':
        $keys = array_keys($entity_array);
        if (in_array('title', $keys)) {
          $in = " in {$entity_array['title']}";
        } elseif (in_array('name', $keys)) {
          $in = " in {$entity_array['name']}";
        } else {
          $in = "";
        }
        $this->log->error("Empty entity type$in");
        break;

      default:
        $storage = \Drupal::entityTypeManager()->getStorage($storage_name);
        break;
    }
    return $storage ?? NULL;
  }

  /**
   * Search for created product.
   */
  private function getEntityFromBase(string $file_path, EntityStorageInterface $storage) {
    if (!empty($this->map[$file_path])) {
      return $storage->load($this->map[$file_path]);
    } else  {
      return NULL;
    }
  }

  /**
   * Search for products_array in $this->directory/products.
   */
  private function getEntityFromFile(string $file_path) {
    $entity_array = Yaml::decode(file_get_contents($this->coreDir . $file_path . '.yml'));
    return $entity_array ?? NULL;
  }

  /**
   * Get all available entity types.
   */
  private function getBundles(array $entity_types) {
    foreach ($entity_types as $entity_type) {
      $bundles[$entity_type] = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);
    }
    return $bundles ?? NULL;
  }

  /**
   * Get all available entity types.
   */
  private function getEntityTypes() {
    return array_keys(\Drupal::entityTypeManager()->getDefinitions()) ?? NULL;
  }

  /**
   * Import TaxonomyField by name or name and vocabulary.
   */
  private function importTaxonomyField($term_name) {
    if (!empty($term_name)) {
      $term_id = $this->getTidByName($term_name);
      return $term_id;
    }
  }

  /**
   * Get taxonomy term id by name or by name and vocabulary id.
   */
  private function getTidByName($name = NULL, $vid = NULL) {
    $properties = [];
    if (!empty($name)) {
      $properties['name'] = $name;
    }
    if (!empty($vid)) {
      $properties['vid'] = $vid;
    }
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties($properties);
    $term = reset($terms);
    return !empty($term) ? $term->id() : 0;
  }

  /**
   * Check already created attributes.
   */
  private function issetAttribute(array $attribute_array) {
    $query = \Drupal::entityQuery('commerce_product_attribute_value')->accessCheck(TRUE);
    if ($attribute_array['attribute'] == 'color') {
      $query->condition('field_hex', $attribute_array['field_hex']);
    } else {
      $query->condition('name', $attribute_array['name']);
    }
    return $query->execute();
  }

  /**
   * Checks vocabulary for existence.
   */
  private function checkVocabularyExistence(string $vid): bool {
    $storage = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');
    if (empty($storage->load($vid))) {
      return FALSE;
    }
    return TRUE;
  }

}
