<?php

namespace Drupal\synimport\Service\Import;

use Drupal\synimport\Service\Logger;

/**
 * Redis class using only for drupal-visitka-telegram and basic fields set.
 *
 * @internal
 *   For internal usage by the synimport module.
 */
class Redis {
  /**
   * Logger variable.
   *
   * @var \Drupal\synimport\Service\Logger
   */
  protected $log;

  /**
   * CreateEntity Service variable.
   *
   * @var CreateEntity
   */
  protected $createEntity;

  /**
   * Files Service variable.
   *
   * @var Files
   */
  protected $filesImport;

  /**
   * Password.
   *
   * @var string
   */
  protected $pass;

  /**
   * Service constructor.
   *
   * @param CreateEntity $createEntity
   *   Service for entity create.
   * @param Files $filesImport
   *   Service for files import.
   */
  public function __construct(CreateEntity $createEntity, Files $filesImport) {
    $this->createEntity = $createEntity;
    $this->filesImport = $filesImport;
    $salt = "pUia>8W9v>";
    $week_number = date("W");
    $pass_string = "{$salt}{$week_number}";
    $this->pass = substr(hash('sha256', $pass_string), 0, 10);
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Import Blocks.
   */
  public function import(string $app_id, string $source_type) {
    $redis_data = [];
    $redis_data = $this->getRedisData($app_id, $source_type);

    if (!empty($redis_data)) {
      if (array_key_exists('nid', $redis_data)) {
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        /** @var \Drupal\node\NodeInterface $created_entity */
        $created_entity = $node_storage->load($redis_data['nid']);
        if ($source_type == 'breaf') {
          $this->createParagraphs($created_entity, $redis_data);
        }
        else {
          $this->updateParagraphs($created_entity, $redis_data);
        }
        $this->log->notice("Node {$redis_data['nid']} updated");
        if (isset($redis_data['h1'])) {
          $created_entity->set('title', $redis_data['h1']);
        }
        if (isset($redis_data['field_short'])) {
          $created_entity->set('field_short', $redis_data['field_short']);
        }
        // if (isset($redis_data['banner_image'])) {
        //   $img = $this->filesImport->importMedia($redis_data['banner_image']);
        //   $created_entity->set('field_image_big', $img);
        // }
        $created_entity->save();
      }
      else {
        if (array_key_exists('type', $redis_data)) {
          $this->log->notice("Try to create entity with type {$redis_data['type']}");
          $this->createEntity->create($redis_data);
        }
        else {
          $this->log->error("Couldn't resolve redis data");
        }
      }
    }
    else {
      $this->log->error('No Redis data found');
    }
  }

  /**
   * Create node paragraphs.
   */
  private function createParagraphs($entity, $redis_data) {
    $paragraph_storage = \Drupal::entityTypeManager()->getStorage('paragraph');
    $paragraphs = $entity->get('field_paragraph')->getValue() ?? [];
    $color_accent = str_replace('#', '', $redis_data['colors']['accent']);
    $img_link = 'https://raw.githubusercontent.com/synapse-studio/helper/refs/heads/master/visitka-telegram/choose_theme/universal/paragraphs/universal.jpg';
    $icon_about = '<img width="80" height="80" src="https://img.icons8.com/dotty/80/' . $color_accent . '/checked.png" alt="checked"/>';
    foreach ($redis_data['paragraphs'] as $key => $paragraph_data) {
      switch ($paragraph_data['keyBlock']) {
        case 'about':
          $about_items = [];
          foreach ($paragraph_data['content'] as $key => $about_item_data) {
            $about_item = $paragraph_storage->create([
              'type' => 'about_item',
              'field_title' => $about_item_data['title'],
              'field_description' => $about_item_data['description'],
              'field_about_icon' => $icon_about,
              // 'field_image' => $this->filesImport->importImages($about_item_data['image'] ?? $img_link),
            ]);
            $about_item->save();
            $about_items[] = [
              'target_id' => $about_item->id(),
              'target_revision_id' => $about_item->getRevisionId(),
            ];
          }
          $paragraph = $paragraph_storage->create([
            'type' => 'about',
            'field_title' => $paragraph_data['title'],
            'field_about_items' => $about_items,
          ]);
          break;

        case 'work_scope':
          $scope_items = [];
          foreach ($paragraph_data['content'] as $key => $work_scope_item_data) {
            $work_scope_item = $paragraph_storage->create([
              'type' => 'work_scope_item',
              'field_title' => $work_scope_item_data['title'],
              'field_short' => $work_scope_item_data['description'],
            ]);
            $work_scope_item->save();
            $scope_items[] = [
              'target_id' => $work_scope_item->id(),
              'target_revision_id' => $work_scope_item->getRevisionId(),
            ];
          }
          $paragraph = $paragraph_storage->create([
            'type' => 'work_scope',
            'field_title' => $paragraph_data['title'],
            'field_work_scope_items' => $scope_items,
          ]);
          break;

        case 'gallery':
          $gallery_items = [];
          foreach ($paragraph_data['content'] as $key => $gallery_item_data) {
            $gallery_item = $paragraph_storage->create([
              'type' => 'image_gallery',
              'field_image' => $this->filesImport->importImages($gallery_item_data['image'] ?? $img_link),
              'field_title' => $gallery_item_data['title'],
              'field_body' => $gallery_item_data['description'],
            ]);
            $gallery_item->save();
            $gallery_items[] = [
              'target_id' => $gallery_item->id(),
              'target_revision_id' => $gallery_item->getRevisionId(),
            ];
          }
          $paragraph = $paragraph_storage->create([
            'type' => 'gallery',
            'field_title' => $paragraph_data['title'],
            'field_gallery_image_text' => $gallery_items,
          ]);
          break;

        case 'gallery_work':
          $gallery_work_gallery = [];
          foreach ($paragraph_data['content'] as $key => $gallery_work_data) {
            $gallery_work_gallery[] = $gallery_work_data['image'] ?? $img_link;
          }
          $paragraph = $paragraph_storage->create([
            'type' => 'gallery_work',
            'field_title' => $paragraph_data['title'],
            'field_gallery' => $this->filesImport->importImages($gallery_work_gallery),
          ]);
          break;

        case 'body':
          if (isset($paragraph_data['content'][0])) {
            $paragraph_data['content'] = $paragraph_data['content'][0];
          }
          $paragraph = $paragraph_storage->create([
            'type' => 'body',
            'field_title' => $paragraph_data['title'],
            'field_body' => $paragraph_data['content']['description'],
          ]);
          break;

        case 'image_blocks':
          $image_blocks_items = [];
          foreach ($paragraph_data['content'] as $key => $image_blocks_item_data) {
            $image_blocks_item = $paragraph_storage->create([
              'type' => 'image_text_sort',
              'field_image' => $this->filesImport->importImages($image_blocks_item_data['image'] ?? $img_link),
              'field_body' => $image_blocks_item_data['description'],
              'field_image_text_sort' => $key % 2 == 0 ? 'text_image' : 'image_text',
            ]);
            $image_blocks_item->save();
            $image_blocks_items[] = [
              'target_id' => $image_blocks_item->id(),
              'target_revision_id' => $image_blocks_item->getRevisionId(),
            ];
          }
          $paragraph = $paragraph_storage->create([
            'type' => 'image_blocks',
            'field_title' => $paragraph_data['title'],
            'field_image_blocks_items' => $image_blocks_items,
          ]);
          break;

        case 'text_image_plate':
          foreach ($paragraph_data['content'] as $key => $text_image_plate_data) {
            $paragraph = $paragraph_storage->create([
              'type' => 'text_image_plate',
              'field_title' => $text_image_plate_data['title'],
              'field_body' => $text_image_plate_data['description'],
              'field_image' => $this->filesImport->importImages($text_image_plate_data['image'] ?? $img_link),
              'field_image_text_sort' => $key % 2 == 0 ? 'text_image' : 'image_text',
            ]);
            $paragraph->save();
            $paragraphs[] = [
              'target_id' => $paragraph->id(),
              'target_revision_id' => $paragraph->getRevisionId(),
            ];
          }
          unset($paragraph);
          break;

        case 'text_slider':
          $text_slider_gallery = [];
          foreach ($paragraph_data['content'] as $key => $gallery_work_data) {
            $text_slider_gallery[] = $gallery_work_data['image'] ?? $img_link;
          }
          $paragraph = $paragraph_storage->create([
            'type' => 'text_slider',
            'field_title' => $paragraph_data['title'],
            'field_body' => $paragraph_data['description'],
            'field_gallery' => $this->filesImport->importImages($text_slider_gallery),
          ]);
          break;

        default:
          unset($paragraph);
          break;
      }
      if (isset($paragraph)) {
        $paragraph->save();
        $paragraphs[] = [
          'target_id' => $paragraph->id(),
          'target_revision_id' => $paragraph->getRevisionId(),
        ];
      }
    }
    $entity->set('field_paragraph', $paragraphs);
    $entity->save();
  }

  /**
   * Add redis data to paragraphs.
   */
  private function updateParagraphs($entity, $redis_data) {
    $paragraph_storage = \Drupal::entityTypeManager()->getStorage('paragraph');
    $paragraphs = $entity->field_paragraph->getValue();
    $image_plate_number = 1;
    foreach ($paragraphs as $paragraph_data) {
      $paragraph = $paragraph_storage->load($paragraph_data['target_id']);
      switch ($paragraph->getType()) {
        case 'about':
          $i = 1;
          if (isset($redis_data["advantages_title"])) {
            $paragraph->set('field_title', $redis_data["advantages_title"]);
            $paragraph->save();
          }
          foreach ($paragraph->field_about_items->getValue() as $about_item_data) {
            $about_item = $paragraph_storage->load($about_item_data['target_id']);
            if (isset($redis_data["advantage$i"])) {
              $about_item->set('field_title', $redis_data["advantage$i"]);
            }
            if (isset($redis_data["advantage_description$i"])) {
              $about_item->set('field_description', $redis_data["advantage_description$i"]);
            }
            if (isset($redis_data["advantage_image$i"])) {
              $img = $this->filesImport->importImages($redis_data["advantage_image$i"] ?? []);
              $about_item->set('field_image', $img);
            }
            $about_item->save();
            $i++;
          }
          break;

        case 'work_scope':
          $i = 1;
          if (isset($redis_data["work_scope_title"])) {
            $paragraph->set('field_title', $redis_data["work_scope_title"]);
            $paragraph->save();
          }
          foreach ($paragraph->field_work_scope_items->getValue() as $work_scope_item_data) {
            $work_scope_item = $paragraph_storage->load($work_scope_item_data['target_id']);
            if (isset($redis_data["work_scope$i"])) {
              $work_scope_item->set('field_title', $redis_data["work_scope$i"]);
            }
            if (isset($redis_data["work_scope_description$i"])) {
              $work_scope_item->set('field_short', $redis_data["work_scope_description$i"]);
            }
            $work_scope_item->save();
            $i++;
          }
          break;

        case 'gallery_work':
          $i = 1;
          if (isset($redis_data["gallery_work_title"])) {
            $paragraph->set('field_title', $redis_data["gallery_work_title"]);
          }
          if (isset($redis_data['gallery_work_gallery'])) {
            $paragraph->set('field_gallery', $this->filesImport->importImages($redis_data['gallery_work_gallery'] ?? []));
          }
          $paragraph->save();
          break;

        case 'image_blocks':
          $image_blocks_item_data = $paragraph->field_image_blocks_items->getValue()[0];
          $image_blocks_item = $paragraph_storage->load($image_blocks_item_data['target_id']);
          if (isset($redis_data['company_info_title'])) {
            $paragraph->set('field_title', $redis_data['company_info_title']);
          }
          if (isset($redis_data['company_info'])) {
            $image_blocks_item->set('field_body', $redis_data['company_info']);
          }
          if (isset($redis_data['company_info_image'])) {
            $image_blocks_item->set('field_image', $this->filesImport->importImages($redis_data['company_info_image'] ?? []));
          }
          $image_blocks_item->save();
          break;

        case 'text_image_plate':
          if (isset($redis_data['text_image_plate_title' . $image_plate_number])) {
            $paragraph->set('field_title', $redis_data["text_image_plate_title" . $image_plate_number]);
          }
          if (isset($redis_data['text_image_plate_description' . $image_plate_number])) {
            $paragraph->set('field_body', $redis_data["text_image_plate_description" . $image_plate_number]);
          }
          if (isset($redis_data['text_image_plate_image' . $image_plate_number])) {
            $paragraph->set('field_image', $this->filesImport->importImages($redis_data["text_image_plate_image" . $image_plate_number] ?? []));
          }
          if ($image_plate_number % 2 == 0) {
            $paragraph->set('field_image_text_sort', 'text_image');
          }
          else {
            $paragraph->set('field_image_text_sort', 'image_text');
          }
          if (isset($redis_data['text_image_plate_title' . $image_plate_number])) {
            $paragraph->save();
          }
          else {
            $paragraph->delete();
          }
          $image_plate_number++;
          break;

        default:
          break;
      }
    }
  }

  /**
   * Load redis data.
   */
  private function getRedisData($app_id, $source_type) {
    switch ($source_type) {
      case 'telegram':
        $host = "https://app.biz-panel.com/{$source_type}/redis-data/{$app_id}/{$this->pass}";
        break;

      case 'breaf':
        $host = "https://biz-panel.com/{$source_type}/redis-data/{$app_id}/{$this->pass}";
        break;
    }
    $url = "$host?" . http_build_query(['json' => 1]);
    $json = file_get_contents($url);
    $data = json_decode(json_encode(json_decode($json)), TRUE);
    if (empty($data)) {
      $this->log->error("No Redis data for $app_id");
    }
    if (array_key_exists('error', $data)) {
      $this->log->error($data['error']);
    }
    return $data;
  }

}
