<?php

namespace Drupal\synimport\Service\Export;

use Drupal\synimport\Service\Logger;

/**
 * Block import class.
 *
 * @internal
 *   For internal usage by the synimport module.
 */
class Block {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * CreateYml Service variable.
   *
   * @var CreateYml
   */
  protected $createYml;

  /**
   * Service constructor.
   *
   * @param CreateYml $createYml
   */
  public function __construct(CreateYml $createYml) {
    $this->createYml = $createYml;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Export blocks.
   */
  public function export(string $export_dir, string $bundle, string | int $status) {
    $this->createYml->setLogger($this->log);
    $this->createYml->createYmls($export_dir, $bundle, $status);
  }

}
