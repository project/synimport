<?php

namespace Drupal\synimport\Drush\Commands;

use Drupal\synimport\Service\Export\Export;
use Drupal\synimport\Service\Import\Import;
use Drupal\synimport\Service\Logger;
use Drupal\synimport\Utility\EmptyDirException;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class SynimportCommands extends DrushCommands {

  /**
   * Logger variable.
   *
   * @var \Drupal\synimport\Service\Logger
   */
  protected $log;

  /**
   * Export Service variable.
   *
   * @var \Drupal\synimport\Service\Export\Export
   */
  protected $synexport;

  /**
   * Import Service variable.
   *
   * @var \Drupal\synimport\Service\Import\Import
   */
  protected $synimport;

  /**
   * Constructs a SynimportCommands object.
   */
  public function __construct(Logger $log, Export $synexport, Import $synimport) {
    $this->log = $log;
    $this->synexport = $synexport;
    $this->synimport = $synimport;
  }

  /**
   * Create a new SynimportCommands object.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('synimport.log'),
      $container->get('synimport.export'),
      $container->get('synimport.import'),
    );
  }

  /**
   * Synapse Content export.
   *
   * @param string $directory
   *   Directory for export.
   *
   * @param string $status
   *   Entities status to export. Default 1.
   *
   * @command synexport
   */
  #[CLI\Command(name: 'synexport', aliases: ['synexport'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for export content.')]
  #[CLI\Argument(name: 'status', description: 'Entity status to export')]
  #[CLI\Usage(name: 'synexport DIRECTORY', description: 'Content export to DIRECTORY.')]
  public function export($directory, $status = '1') {
    $this->synexport->setLogger($this->log);
    $this->synexport->export($directory, $status);
  }

  /**
   * Synapse Taxonomy export.
   *
   * @param string $directory
   *   Directory for export.
   *
   * @param string $status
   *   Terms status to export. Default 1.
   *
   * @command synexport:taxonomy
   */
  #[CLI\Command(name: 'synexport:taxonomy', aliases: ['synexport:taxonomy'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for export taxonomy.')]
  #[CLI\Argument(name: 'status', description: 'Entity status to export')]
  #[CLI\Usage(name: 'synexport:taxonomy DIRECTORY', description: 'Taxonomy export to DIRECTORY.')]
  public function exportTaxonomy($directory, $status = '1') {
    $this->synexport->setLogger($this->log);
    $this->synexport->exportTaxonomy($directory, 'taxonomy_term', $status);
  }

  /**
   * Synapse Nodes export.
   *
   * @param string $directory
   *   Directory for export.
   *
   * @param string $bundle
   *   Node bundle to export. Default 'node'.
   *
   * @param string $status
   *   Node status to export. Default 1.
   *
   * @command synexport:node
   */
  #[CLI\Command(name: 'synexport:node', aliases: ['synexport:node'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for export node.')]
  #[CLI\Argument(name: 'bundle', description: 'Bundle to export')]
  #[CLI\Argument(name: 'status', description: 'Entity status to export')]
  #[CLI\Usage(name: 'synexport:node DIRECTORY', description: 'Node export to DIRECTORY.')]
  public function exportNode($directory, $bundle = 'node', $status = '1') {
    $this->synexport->setLogger($this->log);
    $this->synexport->exportNode($directory, $bundle, $status);
  }

  /**
   * Synapse Products export.
   *
   * @param string $directory
   *   Directory for export.
   *
   * @param string $bundle
   *   Product bundle to export. Default 'commerce_product'.
   *
   * @param string $status
   *   Product status to export. Default 1.
   *
   * @command synexport:product
   */
  #[CLI\Command(name: 'synexport:product', aliases: ['synexport:product'])]
  #[CLI\Argument(name: 'directory', description: 'Directory to export product.')]
  #[CLI\Argument(name: 'bundle', description: 'Bundle to export')]
  #[CLI\Argument(name: 'status', description: 'Entity status to export')]
  #[CLI\Usage(name: 'synexport:product DIRECTORY', description: 'Product export to DIRECTORY.')]
  public function exportProducts($directory, $bundle = 'commerce_product', $status = '1') {
    $this->synexport->setLogger($this->log);
    $this->synexport->exportProducts($directory, $bundle, $status);
  }

  /**
   * Synapse Blocks export.
   *
   * @param string $directory
   *   Directory for export.
   *
   * @param string $bundle
   *   Blocks bundle to export. Default 'block'.
   *
   * @param string $status
   *   Blocks status to export. Default 1.
   *
   * @command synexport:block
   */
  #[CLI\Command(name: 'synexport:block', aliases: ['synexport:block'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for export block.')]
  #[CLI\Argument(name: 'bundle', description: 'Bundle to export')]
  #[CLI\Argument(name: 'status', description: 'Entity status to export')]
  #[CLI\Usage(name: 'synexport:block DIRECTORY', description: 'Block export to DIRECTORY.')]
  public function exportBlocks($directory, $bundle = 'block', $status = '1') {
    $this->synexport->setLogger($this->log);
    $this->synexport->exportBlocks($directory, $bundle, $status);
  }

  /**
   * Synapse Content import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport
   *   Full import from $directory.
   */
  #[CLI\Command(name: 'synimport', aliases: ['synimport'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import content.')]
  #[CLI\Usage(name: 'synimport DIRECTORY', description: 'Content import from DIRECTORY.')]
  public function import($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->setLogger($this->log);
    $this->synimport->import($directory);
  }

  /**
   * Synapse Content import.
   *
   * @param string $app_id
   *   Content directory.
   *
   * @command synimport:redis_import
   *   Full import from $directory.
   */
  #[CLI\Command(name: 'synimport:redis_import', aliases: ['synimport:redis_import'])]
  #[CLI\Argument(name: 'app_id', description: 'App Id.')]
  #[CLI\Argument(name: 'source', description: 'Source type.')]
  #[CLI\Usage(name: 'synimport:redis_import APP_ID', description: 'Content import from Redis.')]
  public function redisImport($app_id, $source) {
    $this->synimport->setLogger($this->log);
    $this->synimport->redisImport($app_id, $source);
  }

  /**
   * Synapse Contacts import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport:contacts
   *   Contacts import from $directory.
   */
  #[CLI\Command(name: 'synimport:contacts', aliases: ['synimport:contacts'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import contacts.')]
  #[CLI\Usage(name: 'synimport:contacts DIRECTORY', description: 'Contacts import from DIRECTORY.')]
  public function importContacts($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->importContacts($directory);
  }

  /**
   * Synapse Taxonomy import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport:taxonomy
   *   Taxonomy import from $directory.
   */
  #[CLI\Command(name: 'synimport:taxonomy', aliases: ['synimport:taxonomy'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import taxonomy.')]
  #[CLI\Usage(name: 'synimport:taxonomy DIRECTORY', description: 'Taxonomy import from DIRECTORY.')]
  public function importTaxonomy($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->setLogger($this->log);
    $this->synimport->importTaxonomy($directory);
  }

  /**
   * Synapse Nodes import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport:node
   *   Nodes import from $directory.
   */
  #[CLI\Command(name: 'synimport:node', aliases: ['synimport:node'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import node.')]
  #[CLI\Usage(name: 'synimport:node DIRECTORY', description: 'Node import from DIRECTORY.')]
  public function importNode($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->setLogger($this->log);
    $this->synimport->importNode($directory);
  }

  /**
   * Synapse Menu import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport:menu
   *   Menu import from $directory.
   */
  #[CLI\Command(name: 'synimport:menu', aliases: ['synimport:menu'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import menu.')]
  #[CLI\Usage(name: 'synimport:menu DIRECTORY', description: 'Menu import from DIRECTORY.')]
  public function importMenu($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->setLogger($this->log);
    $this->synimport->importMenu($directory);
  }

  /**
   * Synapse Products import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport:product
   *   Products import from $directory.
   */
  #[CLI\Command(name: 'synimport:product', aliases: ['synimport:product'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import product.')]
  #[CLI\Usage(name: 'synimport:product DIRECTORY', description: 'Product import from DIRECTORY.')]
  public function importProducts($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->setLogger($this->log);
    $this->synimport->importProducts($directory);
  }

  /**
   * Synapse Blocks import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport:block
   *   Blocks import from $directory.
   */
  #[CLI\Command(name: 'synimport:block', aliases: ['synimport:block'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import block.')]
  #[CLI\Usage(name: 'synimport:block DIRECTORY', description: 'Block import from DIRECTORY.')]
  public function importBlocks($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->setLogger($this->log);
    $this->synimport->importBlocks($directory);
  }

  /**
   * Synapse Synlanding Config import.
   *
   * @param string $directory
   *   Content directory.
   *
   * @command synimport:synlanding_config
   *   Synlanding Config import from $directory.
   */
  #[CLI\Command(name: 'synimport:synlanding_config', aliases: ['synimport:synlanding_config'])]
  #[CLI\Argument(name: 'directory', description: 'Directory for import synlanding config.')]
  #[CLI\Usage(name: 'synimport:synlanding_config DIRECTORY', description: 'Synlanding config import from DIRECTORY.')]
  public function setSynlandingFormBg($directory) {
    $this->dirIsEmpty($directory);
    $this->synimport->setSynlandingFormBg($directory);
  }

  /**
   * Check for empty directory.
   *
   * @throws \Drupal\synimport\Utility\EmptyDirException
   *
   *   No file input.
   */
  private function dirIsEmpty($dirname) {
    if (!file_exists($dirname)) {
      $this->log->send("Not a valid path $dirname");
      throw new EmptyDirException("Not a valid path $dirname");
    }
    if (!is_dir($dirname)) {
      $this->log->send("Not a directory $dirname");
      throw new EmptyDirException("Not a directory $dirname");
    }
    foreach (scandir($dirname) as $file) {
      if (!in_array($file, ['.', '..', '.svn', '.git'])) {
        return FALSE;
      }
    }
    $this->log->send("Dir is empty $dirname");
    throw new EmptyDirException("Dir is empty $dirname");
  }

}
