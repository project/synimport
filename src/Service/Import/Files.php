<?php

namespace Drupal\synimport\Service\Import;

use Drupal\Core\File\FileSystemInterface;
use Drupal\media\Entity\Media;
use Drupal\synimport\Service\Logger;

/**
 * Files import class.
 *
 * @internal
 *   For internal usage by the synimport module.
 */
class Files {
  /**
   * Logger variable.
   *
   * @var Logger
   */
  protected $log;

  /**
   * Import dir variable.
   *
   * @var string
   */
  protected $dir;

  /**
   * Service constructor.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * {@inheritdoc}
   */
  public function setDir(string $dir) {
    $this->dir = $dir;
  }

  /**
   * Import Images.
   */
  public function importImages($images) {
    if (!empty($images)) {
      $files = $this->importFiles($images);
      return $files;
    }
  }

  /**
   * Import Attach.
   */
  public function importAttach($attachFiles) {
    if (!empty($attachFiles)) {
      $i = 0;
      $filenames = [];
      $descriptions = [];
      foreach ($attachFiles as $attach) {
        $filenames[$i] = $attach['source'];
        $descriptions[$i] = $attach['description'];
        $i++;
      }
      $files = $this->importFiles($filenames);
      $i = 0;
      foreach ($files as $k => $file) {
        $files[$k]['description'] = $descriptions[$i];
        $files[$k]['display'] = 1;
        $i++;
      }
      return $files;
    }
  }

  /**
   * Import Media.
   *
   * @TODO: FileSystemInterface::EXISTS_RENAME.
   */
  public function importMedia($filenames) {
    $file_storage = \Drupal::service('entity_type.manager')->getStorage('file');
    $tmp = "/tmp";
    $result = [];
    if (is_string($filenames)) {
      $filenames = [$filenames];
    }
    if (is_array($filenames) && !empty($filenames)) {
      $fs = \Drupal::service('file_system');
      $directory = "public://import/media";
      if (!file_exists($fs->realpath($directory))) {
        \Drupal::service('file_system')->mkdir($directory, NULL, TRUE);
      }
      foreach ($filenames as $fname) {
        $filename = explode("/", $fname);
        $filename = end($filename);
        $f = file_get_contents("$fname");
        $destination = "{$directory}{$filename}";
        $uri = \Drupal::service('file_system')->saveData($f, $destination, FileSystemInterface::EXISTS_REPLACE);
        $file = $file_storage->create([
          'filename' => $filename,
          'uri' => $uri,
          'status' => 1,
        ]);
        $file->save();
        $fid = $file->id();
        $media = Media::create([
          'bundle' => 'image',
          'uid' => 1,
          'field_media_image' => [
            'target_id' => $fid,
          ],
        ]);
        $media->setName($filename)->setPublished(TRUE)->save();
        $mid = $media->id();
      }
      if (!empty($mid)) {
        $result[] = $mid;
      }
    }
    return $result;
  }

  /**
   * Import File.
   *
   * @TODO: FileSystemInterface::EXISTS_RENAME.
   */
  private function importFiles($filenames) {
    $file_storage = \Drupal::service('entity_type.manager')->getStorage('file');
    $tmp = "/tmp";
    $result = [];
    if (is_string($filenames)) {
      $filenames = [$filenames];
    }
    if (is_array($filenames) && !empty($filenames)) {
      $fs = \Drupal::service('file_system');
      $directory = "public://import/";
      if (!file_exists($fs->realpath($directory))) {
        \Drupal::service('file_system')->mkdir($directory, NULL, TRUE);
      }
      foreach ($filenames as $fname) {
        $filename = explode("/", $fname);
        $filename = end($filename);
        if (strpos($fname, 'http') === 0) {
          $f = file_get_contents("$fname");
        }
        else {
          $f = file_get_contents($this->dir . $fname);
        }
        $destination = "{$directory}{$filename}";
        $uri = \Drupal::service('file_system')->saveData($f, $destination, FileSystemInterface::EXISTS_RENAME);
        $file = $file_storage->create([
          'filename' => $filename,
          'uri' => $uri,
          'status' => 1,
        ]);
        $file->save();
        $fid = $file->id();
        if (!empty($fid)) {
          $result[$fid] = [
            'target_id' => $fid,
          ];
        }
      }
    }
    return $result;
  }

}
