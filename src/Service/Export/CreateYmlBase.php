<?php

namespace Drupal\synimport\Service\Export;

use Drupal\file\Entity\File;
use Drupal\synimport\Service\Logger;
use Symfony\Component\Yaml\Yaml;

/**
 * CreateYmlBase class.
 */
class CreateYmlBase {

  /**
   * Logger variable.
   *
   * @var \Drupal\synimport\Service\Logger
   */
  protected $log;

  /**
   * Fields to ignore during export.
   *
   * @var array
   */
  protected $bannedFields;

  /**
   * Service constructor.
   */
  public function __construct(Logger $log) {
    $this->bannedFields = [
      'id',
      'product_id',
      'parent_id',
      'pid',
      'variation_id',
      'attribute_value_id',
      'attribute',
      'nid',
      'vid',
      'node_id',
      'tid',
      'uuid',
      'uid',
      'mid',
      'metatag',
      'path',
      'langcode',
      'default_langcode',
      'behavior_settings',
      'type',
      'list_price',
      'variation_id',
      'weight',
      'parent_type',
      'parent_field_name',
      'stores',
      'created',
      'changed',
    ];
    $this->log = $log;
  }

  /**
   * Creates yml from a single entity.
   */
  public function createYml(string $dir, string $filename, array $yml_data) {
    try {
      if (!file_exists($dir)) {
        mkdir($dir, 0777, TRUE);
      }
      $yaml = Yaml::dump($yml_data);
      file_put_contents("$dir/$filename.yml", $yaml);
    }
    catch (\Throwable $th) {
      $this->log->error("Creating yml error");
      $this->log->error($th->getMessage());
      exit;
    }
  }

  /**
   * Add some data depends on field type.
   */
  public function addTypeFeatures(array $field_data, array &$yml_data, object $entity, string $export_dir) {
    switch ($field_data['type']) {
      case 'taxonomy':
        foreach ($yml_data[$field_data['name']]['content'] as &$term) {
          $term = $term['name'];
        }
        break;

      case 'attribute':
        $field_name = str_replace('attribute_', '', $field_data['name']);
        $content = $yml_data[$field_data['name']]['content'][0];
        $yml_data[$field_name] = $yml_data[$field_data['name']];
        $yml_data[$field_name]['content'] = $content;
        unset($yml_data[$field_data['name']]);
        break;

      case 'variations':
        foreach ($yml_data[$field_data['name']]['content'] as &$variation) {
          $variation['type'] = 'variation';
        }
        break;

      case 'color':
        $hex = $entity->get('field_hex')->getValue()[0];
        $yml_data[$field_data['name']] = $hex['color'];
        break;

      case 'price':
        $price = $entity->getPrice();
        $yml_data[$field_data['name']] = [
          'number' => $price->getNumber(),
          'currency_code' => $price->getCurrencyCode(),
        ];
        break;

      case 'attach':
        $yml_data[$field_data['name']]['content'] = [];
        foreach ($yml_data[$field_data['name']]['files'] as $file_data) {
          $file = File::load($file_data['target_id']);
          $uri = $file->getFileUri();
          $path = str_replace('public://', "sites/default/files/", $uri);
          $file_type = end(explode('.', $path));
          if (!file_exists("$export_dir/files/")) {
            mkdir("$export_dir/files/", 0777, TRUE);
          }
          $new_path = "files/{$file_data['target_id']}-{$field_data['name']}.$file_type";
          copy("/var/www/html/$path", "$export_dir/$new_path");
          $yml_data[$field_data['name']]['content'][] = [
            'source' => "/$new_path",
            'description' => '',
          ];
        }
        unset($yml_data[$field_data['name']]['files']);
        break;

      case 'image':
        $yml_data[$field_data['name']]['content'] = [];
        foreach ($yml_data[$field_data['name']]['files'] as $file_data) {
          $file = File::load($file_data['target_id']);
          $uri = $file->getFileUri();
          $path = str_replace('public://', "sites/default/files/", $uri);
          $file_type = end(explode('.', $path));
          if (!file_exists("$export_dir/files/")) {
            mkdir("$export_dir/files/", 0777, TRUE);
          }
          $new_path = "files/{$file_data['target_id']}-{$field_data['name']}.$file_type";
          copy("/var/www/html/$path", "$export_dir/$new_path");
          $yml_data[$field_data['name']]['content'][] = "/$new_path";
        }
        unset($yml_data[$field_data['name']]['files']);
        break;

      case 'formatted_text':
        $value = $yml_data[$field_data['name']];
        unset($yml_data[$field_data['name']]);
        $yml_data[$field_data['name']] = [
          'format' => 'wysiwyg',
          'value' => $value,
        ];
        break;

      default:
        break;
    }
  }

  /**
   * Check field before usage.
   */
  public function checkBannedField(string $field_name) : bool {
    if (!in_array($field_name, $this->bannedFields) && strpos($field_name, 'revision') === FALSE) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get right field definitions.
   */
  public function fieldDefinitions(string $entity_type, string $bundle) : array {
    $field_definitions = [];
    try {
      $fields_data = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);
      foreach ($fields_data as $field_name => $field) {
        if ($this->checkBannedField($field_name)) {
          $field_definitions[] = $this->fieldSettings($field_name, $field);
        }
      }
    }
    catch (\Throwable $th) {
      $this->log->warning("Couldn't get field definitions");
      $this->log->notice($th->getMessage());
    }
    if (empty($field_definitions)) {
      $this->log->warning("No field definitions found for $bundle");
      $this->log->notice("Empty file wouldn't be created");
    }
    return $field_definitions;
  }

  /**
   * Get right types.
   */
  public function fieldSettings($field_name, $field) : array {
    $type = '';
    $target_type = NULL;
    $is_reference = FALSE;
    $drupal_type = $field->getType();
    $field_settings = $field->getSettings();
    switch ($drupal_type) {
      case 'entity_reference_revisions':
      case 'entity_reference':
        $is_reference = TRUE;
        $target_type = $field_settings['target_type'];
        switch ($target_type) {
          case 'taxonomy_term':
            if ($field_name != 'parent') {
              $type = 'taxonomy';
            }
            else {
              $type = 'related';
            }
            break;

          case 'media':
            $type = 'media';
            break;

          case 'commerce_product_variation':
            $type = 'variations';
            break;

          case 'commerce_product_attribute_value':
            $type = 'attribute';
            break;

          case 'node':
          case 'commerce_product':
            $type = 'related';
            break;

          case 'paragraph':
            $type = 'paragraph';
            break;

          default:
            $type = 'equal_text';
            break;
        }
        break;

      case 'file':
        $type = 'attach';
        break;

      case 'image':
        $type = 'image';
        break;

      case 'commerce_price':
        $type = 'price';
        break;

      case 'text_with_summary':
        $type = 'formatted_text';
        break;

      case 'color_field_type':
        $type = 'color';
        break;

      default:
        $type = 'equal_text';
        break;
    }
    if (in_array($type, ['equal_text', 'color_field_type', 'text_with_summary', 'price'])) {
      $is_reference = FALSE;
    }
    return [
      'name' => $field_name,
      'drupal_type' => $drupal_type,
      'type' => $type,
      'is_reference' => $is_reference,
      'target_type' => $target_type,
    ];
  }

  /**
   * Main data query.
   */
  public function initQuery(string $exporting_entity_type, string $exporting_status, array $exporting_bundles) : object {
    $this->log->notice("Query init...");
    try {
      $query = \Drupal::entityQuery($exporting_entity_type)->accessCheck(FALSE);
      if ($exporting_status !== "0") {
        $query->condition("status", $exporting_status);
      }
      if ($exporting_entity_type != 'taxonomy_term') {
        $query->condition('type', $exporting_bundles, 'IN');
      }
    }
    catch (\Throwable $th) {
      $this->log->error("EntityQuery error");
      $this->log->error($th->getMessage());
      exit;
    }
    return $query;
  }

  /**
   * Get all available entity types.
   */
  public function bundles(array $entity_types) : array | NULL {
    foreach ($entity_types as $entity_type) {
      $bundles[$entity_type] = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);
    }
    return $bundles ?? NULL;
  }

  /**
   * Get all available entity types.
   */
  public function entityTypes() : array | NULL {
    return array_keys(\Drupal::entityTypeManager()->getDefinitions()) ?? NULL;
  }

}
