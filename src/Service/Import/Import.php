<?php

namespace Drupal\synimport\Service\Import;

use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\pathauto\PathautoState;
use Drupal\synimport\Service\Logger;

/**
 * Defines Content Import.
 *
 * @internal
 *   For internal usage by the Commerce synimport module.
 */

/**
 * Import service.
 */
class Import {
  /**
   * Global log variable.
   *
   * @var \Drupal\synimport\Service\Logger
   */
  protected $log;

  /**
   * Files service.
   *
   * @var \Drupal\file\Entity\Files
   */
  protected $filesImport;

  /**
   * Menu service.
   *
   * @var Menu
   */
  protected $menuImport;

  /**
   * Taxonomy service.
   *
   * @var Taxonomy
   */
  protected $taxonomyImport;

  /**
   * Node service.
   *
   * @var Node
   */
  protected $nodeImport;

  /**
   * Product service.
   *
   * @var Product
   */
  protected $productImport;

  /**
   * Block service.
   *
   * @var Block
   */
  protected $blockImport;

  /**
   * Redis service.
   *
   * @var Redis
   */
  protected $redisImport;

  /**
   * Service constructor.
   *
   * @param \Drupal\file\Entity\Files $filesImport
   * @param Menu $menuImport
   * @param Taxonomy $taxonomyImport
   * @param Node $nodeImport
   * @param Product $productImport
   * @param Block $blockImport
   * @param Redis $redisImport
   */
  public function __construct(
    Files $filesImport,
    Menu $menuImport,
    Taxonomy $taxonomyImport,
    Node $nodeImport,
    Product $productImport,
    Block $blockImport,
    Redis $redisImport,
  ) {
    $this->filesImport = $filesImport;
    $this->menuImport = $menuImport;
    $this->taxonomyImport = $taxonomyImport;
    $this->nodeImport = $nodeImport;
    $this->productImport = $productImport;
    $this->blockImport = $blockImport;
    $this->redisImport = $redisImport;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(Logger $log) {
    $this->log = $log;
  }

  /**
   * Import Full Content.
   */
  public function import(string $directory) {
    if (!$this->dirIsEmpty($directory . '/contacts', "Contacts")) {
      $this->log->head('Start Contacts Import');
      $this->importContacts($directory . '/contacts');
      $this->log->success('Done Contacts Import');
    }

    if (!$this->dirIsEmpty($directory . '/menu', "Menu")) {
      $this->log->head('Start Menu Import');
      $this->menuImport->setLogger($this->log);
      $this->menuImport->import($directory . '/menu');
      $this->log->success('Done Menu Import');
    }

    if (!$this->dirIsEmpty($directory . '/taxonomy', "Taxonomy")) {
      $this->log->head('Start Taxonomy Import');
      $this->taxonomyImport->setLogger($this->log);
      $this->taxonomyImport->import($directory . '/taxonomy');
      $this->log->success('Done Taxonomy Import');
    }

    if (!$this->dirIsEmpty($directory . '/nodes', "Node")) {
      $this->log->head('Start Node Import');
      $this->nodeImport->setLogger($this->log);
      $this->nodeImport->import($directory . '/nodes');
      $this->log->success('Done Node Import');
    }

    if (!$this->dirIsEmpty($directory . '/products', "Product")) {
      $this->log->head('Start Product Import');
      $this->productImport->setLogger($this->log);
      $this->productImport->import($directory . '/products');
      $this->log->success('Done Product Import');
    }

    if (!$this->dirIsEmpty($directory . '/block', "Block")) {
      $this->log->head('Start Block Import');
      $this->blockImport->setLogger($this->log);
      $this->blockImport->import($directory . '/block');
      $this->log->success('Done Block Import');
    }

    if (!$this->dirIsEmpty($directory . '/synlanding', "Synlanding Configs")) {
      $this->log->head('Start Setting Synlanding Configs');
      $this->setSynlandingFormBg($directory . '/synlanding');
      $this->log->success('Done Setting Synlanding Configs');
    }
  }

  /**
   *
   */
  public function redisImport($app_id, $source_type) {
    $this->log->head('Start Import from Redis');
    $this->redisImport->setLogger($this->log);
    $this->redisImport->import($app_id, $source_type);
    $this->log->success('Done Import from Redis');
  }

  /**
   * Import Menu.
   */
  public function importMenu(string $directory) {
    $this->log->head('Start Menu Import');
    $this->menuImport->setLogger($this->log);
    $this->menuImport->import($directory);
    $this->log->success('Done Menu Import');
  }

  /**
   * Import Taxonomy.
   */
  public function importTaxonomy(string $directory) {
    $this->log->head('Start Taxonomy Import');
    $this->taxonomyImport->setLogger($this->log);
    $this->taxonomyImport->import($directory);
    $this->log->success('Done Taxonomy Import');
  }

  /**
   * Import Node.
   */
  public function importNode(string $directory) {
    $this->log->head('Start Node Import');
    $this->nodeImport->setLogger($this->log);
    $this->nodeImport->import($directory);
    $this->log->success('Done Node Import');
  }

  /**
   * Import Products.
   */
  public function importProducts(string $directory) {
    $this->log->head('Start Product Import');
    $this->productImport->setLogger($this->log);
    $this->productImport->import($directory);
    $this->log->success('Done Product Import');
  }

  /**
   * Import Blocks.
   */
  public function importBlocks(string $directory) {
    $this->log->head('Start Block Import');
    $this->blockImport->setLogger($this->log);
    $this->blockImport->import($directory);
    $this->log->success('Done Block Import');
  }

  /**
   * Import Contacts.
   */
  public function importContacts(string $directory) {
    $sourceContacts = scandir($directory);
    foreach ($sourceContacts as $file_name) {
      if (!is_dir($file_name)) {
        if (mb_substr($directory, -1) == '/') {
          $path = $directory . $file_name;
        }
        else {
          $path = $directory . '/' . $file_name;
        }
        $data = Yaml::decode(file_get_contents($path));
        $path = \Drupal::service('path_alias.manager')->getPathByAlias('/contacts');
        if ($path == '/contacts') {
          $path = \Drupal::service('path_alias.manager')->getPathByAlias('/kontakty');
        }
        $params = Url::fromUri("internal:" . $path)->getRouteParameters();
        $entity_type = key($params);
        $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);
        $node->body->format = $data['body']['format'];
        $node->body->value = $data['body']['value'];
        $node->path = [
          'alias' => $data['path'],
          'pathauto' => PathautoState::SKIP,
        ];
        $node->save();
      }
    }
  }

  /**
   * Set Synlanding form background.
   */
  public function setSynlandingFormBg(string $directory) {
    $sourceSynLanding = scandir($directory);
    foreach ($sourceSynLanding as $file_name) {
      if (!is_dir($file_name)) {
        if (mb_substr($directory, -1) == '/') {
          $path = $directory . $file_name;
        }
        else {
          $path = $directory . '/' . $file_name;
        }
        $data = Yaml::decode(file_get_contents($path));
        $form_bg_path = $data['form_bg_path'] ?? '';
        if (!$form_bg_path) {
          continue;
        }
        $this->filesImport->setDir($directory);
        $imageId = $this->filesImport->importImages($data['form_bg_path']);
        $idFile = array_shift($imageId)['target_id'];
        \Drupal::configFactory()
          ->getEditable($data['config_id'])
          ->set('form_bg', [$idFile])
          ->save();
        $image = File::load($idFile);
        $file_usage = \Drupal::service('file.usage');
        $file_usage->add($image, 'synlanding', 'file', $idFile);
      }
    }
  }

  /**
   * Check for empty directory.
   *
   * @throws Drupal\synimport\Classes\EmptyDirException
   *
   *   No file input.
   */
  private function dirIsEmpty(string $dirname, string $dirtype) {
    if (!file_exists($dirname)) {
      $this->log->warning("No $dirtype directory");
      return TRUE;
    }
    foreach (scandir($dirname) as $file) {
      if (!in_array($file, ['.', '..', '.svn', '.git'])) {
        return FALSE;
      }
    }
    $this->log->warning("$dirtype is empty");
    return TRUE;
  }

}
